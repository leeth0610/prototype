﻿using System;
using UnityEngine;

public interface IWeapon
{
    void Init();
    void SetHolder(GameObject holder);
    void Attack(Collider otherObject, Vector3 collisionPoint, Vector3 collisionNormal);
    void SetOnHitEventHandler(Action<Collision> action);
    void UpdateAttackState(bool attackState);
}
