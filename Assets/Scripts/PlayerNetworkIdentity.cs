﻿using System;
using Characters;
using Mirror;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UIElements;

namespace DefaultNamespace
{
    public class PlayerNetworkIdentity : NetworkBehaviour
    {
        private Material _hairMat;
        public int HairMaterialIndex = 3;
        public Color RemotePlayerHairColor;
        public Color LocalPlayerHairColor;
        private void Start()
        {
            var renderer= gameObject.GetComponentInChildren<Renderer>();
            var mats = renderer.materials;
            
            _hairMat = mats[HairMaterialIndex];
            if (isLocalPlayer)
            {
                _hairMat.SetColor("_Color", LocalPlayerHairColor);
            }
            else
            {
                _hairMat.SetColor("_Color", RemotePlayerHairColor);
            }
        }
    }
}