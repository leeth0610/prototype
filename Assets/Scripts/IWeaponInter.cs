﻿using UnityEngine;

public interface IWeaponInter
{
    void OnWeaponHit(int damage, Vector3 point, Vector3 normal, float force);
}