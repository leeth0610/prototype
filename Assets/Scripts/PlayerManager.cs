﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Mirror;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UIElements;

//Update, Flag Managements.
public class PlayerManager : NetworkBehaviour
{
    private InputHandler _inputHandler;
    private PlayerActions _playerActions;
    private PlayerCameraController _playerCameraController;


    [HideInInspector] public bool isAttacking;
    [HideInInspector] public bool isGuarding;
    [HideInInspector] public bool isEvade;
    [HideInInspector] public bool canMove = true;
    [HideInInspector] public Transform lockOn;

    private void Start()
    {
        _inputHandler = GetComponent<InputHandler>();
        _playerActions = GetComponent<PlayerActions>();
        _playerCameraController = PlayerCameraController.Instance;
    }

    public void Update()
    {
        float delta = Time.deltaTime;
        _inputHandler.TickInput(delta);
        _playerActions.HandleMovement(delta);
        _playerActions.HandleRotation(delta);
        _playerActions.HandleAttack(delta);
        _playerActions.HandleGuard(delta);
        _playerActions.HandleEvade(delta);
        _playerActions.HandleLockOn(delta);
        _playerActions.HandleImpulse(delta);
        _playerActions.ForwardTarget(delta);
    }

    private void LateUpdate()
    {
        float delta = Time.deltaTime;
        if (isLocalPlayer)
        {
            _playerCameraController.MouseXDelta = _inputHandler.MouseX;
            _playerCameraController.MosueYDelta = _inputHandler.MouseY;
            _playerCameraController.LookOn(transform, lockOn, delta);
        }
    }
}