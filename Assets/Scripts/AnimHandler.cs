﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Mirror;
using UnityEngine;
using Random = UnityEngine.Random;

public class AnimHandler : MonoBehaviour, AnimatorEvent.IAnimatorEventHandler
{
    public int startParringIndex = 0;
    public int startHitIndex = 0;
    public int maxParringIndex = 1;
    public int maxHitIndex = 1;
    public float movementSmoothBlend = 0.1f;
    [HideInInspector] public Animator animator;
    [HideInInspector] public NetworkAnimator nanimator;

    public delegate void OnStateChangedFn(AnimatorEvent.StateName stateName, AnimatorEvent.StateChangeEvent e,
        AnimatorStateInfo stateInfo, int layerIndex);

    private Action<AnimatorEvent.StateName, AnimatorEvent.StateChangeEvent,
        AnimatorStateInfo, int> _onStateChangedFns;

    public void Init()
    {
        animator = GetComponent<Animator>();
        nanimator = GetComponent<NetworkAnimator>();
    }

    public void UpdateMovementValues(float moveSpeed, Vector3 dir)
    {
        animator.SetFloat("MoveSpeed", moveSpeed, movementSmoothBlend, Time.deltaTime);
    }

    public void Attack()
    {
        if (nanimator) nanimator.SetTrigger("Attacking");
        else animator.SetTrigger("Attacking");
    }

    public void Guard(bool b)
    {
        animator.SetBool("Guarding", b);
    }

    public void Hit(bool isParrying)
    {
        if (isParrying)
        {
            animator.SetInteger("ParryingIndex", startParringIndex);
            if (nanimator) nanimator.SetTrigger("Parrying");
            else animator.SetTrigger("Parrying");
            if (startParringIndex >= maxParringIndex)
                startParringIndex = 0;
            else startParringIndex++;
        }
        else
        {
            animator.SetInteger("HitIndex", startHitIndex);
            if (nanimator) nanimator.SetTrigger("Hit");
            else animator.SetTrigger("Hit");
            if (startHitIndex >= maxHitIndex)
                startHitIndex = 0;
            else startHitIndex++;
        }
    }

    public void Evade()
    {
        if (nanimator) nanimator.SetTrigger("Evade");
        else animator.SetTrigger("Evade");
        
    }

    public void SetOnStateChange(
        Action<AnimatorEvent.StateName, AnimatorEvent.StateChangeEvent, AnimatorStateInfo, int> fn)
    {
        _onStateChangedFns += fn;
    }

    public void OnStateChanged(AnimatorEvent.StateName stateName, AnimatorEvent.StateChangeEvent e,
        AnimatorStateInfo stateInfo, int layerIndex)
    {
        _onStateChangedFns.Invoke(stateName, e, stateInfo, layerIndex);
    }
}