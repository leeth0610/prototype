﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Authentication.ExtendedProtection;
using System.Security.Cryptography;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputHandler : MonoBehaviour
{
    public float horizontal;
    public float vertical;
    public bool isCrawling;
    public bool isAttacking;
    public bool isGuarding;
    public bool isEvade;
    public bool isLockon;
    public float MouseX
    {
        get => _cameraInput.x;
    }

    public float MouseY
    {
        get => _cameraInput.y;
    }
    
    private PlayerControls _inputActions;
    private Vector2 _movementInput;
    private Vector2 _cameraInput;

    private void OnEnable()
    {
        if (_inputActions == null)
        {
            _inputActions = new PlayerControls();
            _inputActions.PlayerMovement.Movement.performed +=
                _inputActions => _movementInput = _inputActions.ReadValue<Vector2>();
            _inputActions.PlayerMovement.Camera.performed += i => _cameraInput = i.ReadValue<Vector2>();
        }
        _inputActions.Enable();
    }

    private void OnDisable()
    {
            _inputActions.Disable();
    }

    public void TickInput(float delta)
    {
        MoveInput(delta);
        CrawlInput(delta);
        AttackInput(delta);
        GuardInput(delta);
        EvadeInput(delta);
        LockOnInput(delta);
    }

    public void MoveInput(float delta)
    {
        horizontal = _movementInput.x;
        vertical = _movementInput.y;
    }

    public void CrawlInput(float delta)
    {
        bool started = _inputActions.PlayerActions.Crawl.phase == UnityEngine.InputSystem.InputActionPhase.Started;
        if (started)
        {
            isCrawling = true;
        }
        else
        {
            isCrawling = false;
        }
    }

    public void AttackInput(float delta)
    {
        bool started = _inputActions.PlayerActions.Attack.phase == UnityEngine.InputSystem.InputActionPhase.Started;
        if (started)
        {
            isAttacking = true;
        }
        else
        {
            isAttacking = false;
        }
    }

    public void GuardInput(float delta)
    {
        bool started = _inputActions.PlayerActions.Guard.phase == InputActionPhase.Started;
        bool waiting = _inputActions.PlayerActions.Guard.phase == InputActionPhase.Waiting;
        
        if (started)
        {
            isGuarding = true;
        }
        else if (waiting)
        {
            isGuarding = false;
        }
    }

    public void EvadeInput(float delta)
    {
        bool started = _inputActions.PlayerActions.Evade.phase == InputActionPhase.Started;
        if (started)
        {
            isEvade = true;
        }
        else
        {
            isEvade = false;
        }
    }

    public void LockOnInput(float delta)
    {
        bool triggered = _inputActions.PlayerActions.LockOn.triggered;
        if (triggered)
        {
            isLockon = !isLockon;
        }
    }
}
