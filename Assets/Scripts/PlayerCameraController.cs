﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Serialization;

public sealed class PlayerCameraController : MonoBehaviour
{
    public static PlayerCameraController Instance;
    [HideInInspector]
    public Transform pivotTransform;
    [HideInInspector]
    public Transform defaultTransform;
    [FormerlySerializedAs("SmoothTime")] [Header("Stat")]
    public float PositionSmoothTime = 0.08f; 
    public float LookSlerpSpeed = 15f; 
    private Vector3 velocity = Vector3.zero;
    public float LookSpeed = 0.05f;
    public Transform cameraTransform;
    [HideInInspector]
    public float MouseXDelta;
    [HideInInspector]
    public float MosueYDelta;

    private float _minimumY = -35;
    private float _maximumY = 35;
    private float _xAngle;
    private float _yAngle;

    private void Awake()
    {
        Instance = this;
    }

    public void LockOff(Transform ownerTransform, float delta)
    {
        _xAngle += (MouseXDelta * LookSpeed) / delta;
        _yAngle -= (MosueYDelta * LookSpeed) / delta;
        _yAngle = Mathf.Clamp(_yAngle, _minimumY, _maximumY);
        Vector3 rotation = Vector3.zero;
        rotation.y = _xAngle;
        rotation.x = _yAngle;
        transform.rotation = Quaternion.Euler(rotation);
        HandleCameraCollisions(delta);
    }
    
    private void HandleCameraCollisions(float delta)
    {
       //Todo
    }
    
    public void LookOn(Transform ownerTransform, Transform pTarget, float delta)
    {
        if (ownerTransform == null)
        {
            ownerTransform = defaultTransform;
        }

        transform.position = Vector3.SmoothDamp(transform.position, ownerTransform.position, ref velocity, PositionSmoothTime);
        
        Quaternion targetRotation;
        if (pTarget != null)
        {
            targetRotation = Quaternion.LookRotation(pTarget.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, LookSlerpSpeed * delta);
        }
        else
        {
            LockOff(ownerTransform, delta);
        }
    }
}
