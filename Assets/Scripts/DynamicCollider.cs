﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Mono.CecilX;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering;

public class DynamicCollider : MonoBehaviour
{
    private class ControllPoint
    {
        public Vector3 StartPosition;
        public Vector3 EndPosition;
    }

    [ItemCanBeNull] private Queue<ControllPoint> _ctpChain;
    public Transform StartPosition;
    public Transform EndPosition;
    public Material Material;
    public int MaxMeshSegment = 10;
    public int LimitControllPoint = 10;

    private bool _enable;
    private int _minimalMeshCount = 3;
    private float _deleteTime;
    private GameObject _meshOwner;
    private MeshRenderer _renderer;
    private MeshFilter _filter;
    private float _interval = 1.0f / 60.0f;
    private float _elap = 0.0f;
    private Stack<ControllPoint> _ccache;
    private readonly int _splineFactor = 1;

    // Start is called before the first frame update
    void Start()
    {
        _meshOwner = new GameObject("DY_MESH");
        _filter = _meshOwner.AddComponent<MeshFilter>();
        _renderer = _meshOwner.AddComponent<MeshRenderer>();
        _renderer.shadowCastingMode = ShadowCastingMode.Off;
        _renderer.receiveShadows = false;
        _renderer.sharedMaterial = Material;
        _renderer.enabled = false;
        _ctpChain = new Queue<ControllPoint>();
        _ccache = new Stack<ControllPoint>();
        for (int i = 0; i < LimitControllPoint + 1; i++)
        {
            _ccache.Push(new ControllPoint());
        }
    }

    private void _AddPoint()
    {
        var ctp = _ccache.Pop();
        ctp.StartPosition = StartPosition.position;
        ctp.EndPosition = EndPosition.position;
        _ctpChain.Enqueue(ctp);
        if (_ctpChain.Count > LimitControllPoint)
        {
            var c = _ctpChain.Dequeue();
            _ccache.Push(c);
        }
    }

    private void _DeletePoint()
    {
        if (_ctpChain.Count > 0)
        {
            var c = _ctpChain.Dequeue();
            _ccache.Push(c);
        }
    }

    public void Enable(bool enable)
    {
        _Clear();
        _enable = enable;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (_enable && MaxMeshSegment > 0)
        {
            _elap += Time.deltaTime;
            if (_elap > _interval)
            {
                _elap = 0.0f;
                _AddPoint();
            }
        }
        else
        {
            _DeletePoint();
        }
        _MakeMesh();
    }

    Vector3 _HermitSpline(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        Vector3 a = 2f * p1;
        Vector3 b = p2 - p0;
        Vector3 c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
        Vector3 d = -p0 + 3f * p1 - 3f * p2 + p3;
        Vector3 pos = 0.5f * (a + (b * t) + (c * t * t) + (d * t * t * t));
        return pos;
    }

    void _MakeMesh()
    {
        if (_ctpChain.Count >= _splineFactor)
        {
            List<ControllPoint> lCp = new List<ControllPoint>();
            //Using Hermite Curv
            var chains = _ctpChain.ToArray();
            for (int j = 0; j < chains.Length - 1; j++)
            {
                for (int i = 0; i < MaxMeshSegment; i++)
                {
                    ControllPoint nC = new ControllPoint();

                    int prev = j;
                    int current = j + 1;
                    int next = j + 2;
                    int next2 = j + 3;

                    if (j + 1 >= chains.Length)
                        current = j;
                    if (j + 2 >= chains.Length)
                        next = current;
                    if (j+3 >= chains.Length)
                        next2 = next;
                    
                    nC.StartPosition = _HermitSpline(chains[prev].StartPosition, chains[current].StartPosition,
                        chains[next].StartPosition, chains[next2].StartPosition, (i / (float) MaxMeshSegment));
                    nC.EndPosition = _HermitSpline(chains[prev].EndPosition, chains[current].EndPosition,
                        chains[next].EndPosition,
                        chains[next2].EndPosition, (i / (float) MaxMeshSegment));
                    lCp.Add(nC);
                }
            }

            var vertices = new Vector3[2 * lCp.Count];
            var indices = new int[6 * (lCp.Count - 1)];
            var uv = new Vector2[vertices.Length];
            int idx = 0;
            foreach (var controllPoint in lCp)
            {
                if (idx < indices.Length / 6)
                {
                    int i = idx;
                    indices[0 + (6 * i)] = 0 + (i * 2);
                    indices[1 + (6 * i)] = 1 + (i * 2);
                    indices[2 + (6 * i)] = 2 + (i * 2);
                    indices[3 + (6 * i)] = 2 + (i * 2);
                    indices[4 + (6 * i)] = 1 + (i * 2);
                    indices[5 + (6 * i)] = 3 + (i * 2);
                }

                vertices[0 + (2 * idx)] = controllPoint.StartPosition;
                vertices[1 + (2 * idx)] = controllPoint.EndPosition;
                uv[0 + (2 * idx)] = new Vector2(1 - (idx / (float) (lCp.Count - 1)), 1);
                uv[1 + (2 * idx)] = new Vector2(1 - (idx / (float) (lCp.Count - 1)), 0);
                idx++;
            }

            var mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = indices;
            mesh.uv = uv;
            mesh.name = "DM_COLLIDER_MESH";
            mesh.RecalculateNormals();
            _filter.mesh = mesh;
            _renderer.enabled = true;
        }
        else
        {
            _renderer.enabled = false;
        }
    }

    void _Clear()
    {
        for (int i = 0; i < _ctpChain.Count; i++)
        {
            var c = _ctpChain.Dequeue();
            _ccache.Push(c);
        }
        _deleteTime = 0.0f;
    }
}