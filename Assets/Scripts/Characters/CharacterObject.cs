﻿using System;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

namespace Characters
{
    public class CharacterObject : NetworkBehaviour
    {
        [SyncVar]
        private int _hp = 100;

        [SyncVar]
        private int _sp = 100;
        
        private bool _attackable = false;

        private int _maxHp = 100;
        private int _maxSp = 100;
        private int _evadeSp = 50;
        private int _attackSp = 15;
        private int _spRecover = 1;

        public virtual (bool, int) Hit(int damage, Vector3 point, Vector3 normal, float force)
        {
            _hp -= damage;
            var playerActions = GetComponent<IWeaponInter>();
            playerActions.OnWeaponHit(damage, point, normal, force);
            return (true, damage);
        }

        private void Update()
        {
            if (Dead())
            {
                Destroy(gameObject);
            }
        }

        public virtual int Hp()
        {
            return _hp;
        }

        public virtual int MaxHp()
        {
            return _maxHp;
        }

        public virtual int Sp()
        {
            return _sp;
        }

        public virtual void SetSp(int sp)
        {
            if (sp > _maxSp) sp = _maxSp;
            _sp = sp;
        }

        public virtual int MaxSp()
        {
            return _maxSp;
        }

        public virtual int EvadeSp()
        {
            return _evadeSp;
        }

        public virtual int AttackSp()
        {
            return _attackSp;
        }

        public virtual int SpRecover()
        {
            return _spRecover;
        }

        public virtual bool Dead()
        {
            return _hp <= 0;
        }

        public void SetAttackable(bool b)
        {
            _attackable = b;
        }

        public bool GetAttackable()
        {
            return _attackable;
        }
    }
}