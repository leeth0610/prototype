﻿using Characters;
using NUnit.Framework.Constraints;
using UnityEngine;

public class BasicCharacterObject: CharacterObject
{
    public override (bool, int) Hit(int damage, Vector3 point, Vector3 normal, float force)
    {
        if (base.GetAttackable())
        {
            Debug.Log("Evade!");
            return (false, 0);
        }
        
        return base.Hit(damage, point, normal, force);
    }
}