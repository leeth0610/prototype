﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters;
using Mirror;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PlayerActions : Mirror.NetworkBehaviour, IWeaponInter
{
    [HideInInspector] public AnimHandler animHandler;
    [Header("Stats")] [SerializeField] private float crawlSpeed = 13;
    [Header("Stats")] [SerializeField] private float movementSpeed = 30;
    [Header("Stats")] [SerializeField] private float rotationSpeed = 10;
    [Header("Effect")] public ParticleSystem BloodEffect;
    [Header("Effect")] public ParticleSystem ParryingEffect;
    [FormerlySerializedAs("HitableAngle")] public float ParryableAngle = 80.0f;

    private CharacterObject _character;
    public GameObject weaponHolder;

    private GameObject _weapon;
    private IWeapon _iweapon;

    private PlayerManager _playerManager;
    private InputHandler _inputHandler;
    private Vector3 _moveDirection;
    private CharacterController _characterController;
    private PlayerCameraController _playerCameraController;
    private bool _atkLock;
    private Vector3 _impulse;
    private AnimatorEvent.StateName _currAnim;


    private void Start()
    {
        _currAnim = AnimatorEvent.StateName.Locomo;
        _playerCameraController = PlayerCameraController.Instance;
        _playerManager = GetComponent<PlayerManager>();
        _inputHandler = GetComponent<InputHandler>();
        _characterController = GetComponent<CharacterController>();
        _characterController.isTrigger = false;
        animHandler = GetComponentInChildren<AnimHandler>();

        animHandler.Init();
        animHandler.SetOnStateChange(_onAnimationStateChanged);

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            var child = gameObject.transform.GetChild(i);
            if (child.CompareTag("Weapon"))
            {
                _weapon = child.gameObject;
            }
        }

        //_weapon = GameObject.FindGameObjectWithTag("Weapon");
        _iweapon = _weapon.GetComponent<IWeapon>();
        _iweapon.SetHolder(this.weaponHolder);

        if (isServer)
        {
            _iweapon.SetOnHitEventHandler(_onWeaponHit);
        }

        _iweapon.Init();

        _character = GetComponent<CharacterObject>();

        if (!isLocalPlayer)
        {
            animHandler.animator.applyRootMotion = false;
        }
        else
        {
            animHandler.animator.applyRootMotion = true;
        }
    }

    public void Update()
    {
        _updateUI();
        _recoverSp();
    }

    private float _spRecoverGauge = .0f;
    private void _recoverSp()
    {
        _spRecoverGauge += Time.deltaTime;
        if (_spRecoverGauge > 0.05)
        {
            _spRecoverGauge -= 0.05f;
            _character.SetSp(_character.Sp() + _character.SpRecover());
        }
    }

    private void _onWeaponHit(Collision collision)
    {
        if (_atkLock)
        {
            return;
        }

        var collider = collision.collider;
        Debug.Log("ME:" + gameObject.name);
        Debug.Log("YU:" + collider.GetComponent<Collider>().gameObject.name);
        if (collider.GetComponent<Collider>().gameObject.Equals(gameObject))
        {
            return;
        }

        _atkLock = true;
        var contact = collision.GetContact(0);
        _iweapon.Attack(collider, contact.point, contact.normal);
    }

    private void OnHitStateChanged(int b)
    {
        if (!isServer) return;
        _atkLock = !(b > 0);
    }

    private void OnEffectStateChanged(int b)
    {
        if (!isServer) return;
        _iweapon.UpdateAttackState(b > 0); 
    }

    private void OnEvadeStateChanged(int b)
    {
        if (!isServer) return;
        bool evading = b > 0;

        _character.SetAttackable(evading);
    }

    private void _onAnimationStateChanged(AnimatorEvent.StateName stateName, AnimatorEvent.StateChangeEvent e,
        AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!isLocalPlayer) return;
        if (AnimatorEvent.StateChangeEvent.OnEnter == e)
        {
            _currAnim = stateName;
        }
        bool isCanBusy = stateName == AnimatorEvent.StateName.Attack || stateName == AnimatorEvent.StateName.Guard || stateName == AnimatorEvent.StateName.Hit;

        if (stateName == AnimatorEvent.StateName.Locomo && e == AnimatorEvent.StateChangeEvent.OnEnter)
        {
            _playerManager.canMove = true;
        }
        else if (isCanBusy && e == AnimatorEvent.StateChangeEvent.OnEnter)
        {
            _playerManager.canMove = false;
        }
    }

    public void HandleMovement(float delta)
    {
        if (!isLocalPlayer) return;



        var inputDirection = new Vector3(_inputHandler.horizontal, 0, _inputHandler.vertical);
        float rate = _playerCameraController.transform.eulerAngles.y;
        var RadianRate = rate * Mathf.Deg2Rad;

        var rotationAxis = new Vector3(0, 1.0f, 0);
        var qu = new Quaternion(Mathf.Sin(RadianRate / 2.0f) * rotationAxis.x,
            Mathf.Sin(RadianRate / 2.0f) * rotationAxis.y, Mathf.Sin(RadianRate / 2.0f) * rotationAxis.z,
            Mathf.Cos(RadianRate / 2.0f)).normalized;
        var quC = new Quaternion(-Mathf.Sin(RadianRate / 2.0f) * rotationAxis.x,
            -Mathf.Sin(RadianRate / 2.0f) * rotationAxis.y, -Mathf.Sin(RadianRate / 2.0f) * rotationAxis.z,
            Mathf.Cos(RadianRate / 2.0f)).normalized;
        var finalRotation = qu * new Quaternion(inputDirection.x, inputDirection.y, inputDirection.z, 0) * quC;

        _moveDirection = new Vector3(finalRotation.x, finalRotation.y, finalRotation.z);
        _moveDirection.Normalize();
        if (_inputHandler.isCrawling)
        {
            _moveDirection *= crawlSpeed;
        }
        else
        {
            _moveDirection *= movementSpeed;
        }

        animHandler.UpdateMovementValues(_moveDirection.magnitude, _moveDirection.normalized);
        if (_playerManager.canMove == false)
        {
            return;
        }
        _characterController.Move(_moveDirection * delta);
    }

    public void HandleRotation(float delta)
    {
        if (!isLocalPlayer) return;
        if (_playerManager.canMove == false)
        {
            return;
        }
 
        Vector3 targetDir = _moveDirection;
        if (_moveDirection == Vector3.zero)
        {
            targetDir = transform.forward;
        }

        Quaternion tr = Quaternion.LookRotation(targetDir);
        Quaternion targetRotation = Quaternion.Slerp(transform.rotation, tr, rotationSpeed * delta);
        transform.rotation = targetRotation;
    }

    public void HandleAttack(float delta)
    {
        if (!isLocalPlayer) return;
        if (!_inputHandler.isAttacking)
        {
            _playerManager.isAttacking = false;
            return;
        }

        if (_playerManager.isAttacking == false)
        {
            _playerManager.isAttacking = true;
            _character.SetSp(_character.Sp()-_character.AttackSp());
            animHandler.Attack();
        }
    }

    public void HandleGuard(float delta)
    {
        if (!isLocalPlayer) return;
        if (_inputHandler.isGuarding)
        {
            animHandler.Guard(true);
        }
        else
        {
            animHandler.Guard(false);
        }

        _playerManager.isGuarding = _inputHandler.isGuarding;
    }

    public void HandleEvade(float delta)
    {
        if (!isLocalPlayer) return;
        if (!_inputHandler.isEvade)
        {
            _playerManager.isEvade = false;
            return;
        }

        if (_playerManager.isEvade == false)
        {
            if (_character.Sp() < _character.EvadeSp()) return;
            
            _playerManager.isEvade = true;
            _character.SetSp(_character.Sp()-_character.EvadeSp());
            animHandler.Evade();
        }
    }

    public Transform GetRemotePlayerTransform()
    {
        var objects = FindObjectsOfType<NetworkBehaviour>();
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].isLocalPlayer == false)
            {
                return objects[i].transform;
            }
        }

        return null;
    }

    public void HandleLockOn(float delta)
    {
        if (!isLocalPlayer) return;
        if (!_inputHandler.isLockon)
        {
            _playerManager.lockOn = null;
            return;
        }

        if (_playerManager.lockOn == null)
        {
            _playerManager.lockOn = GetRemotePlayerTransform();
        }
    }

    private void _updateUI()
    {
        // HP 업데이트
        var hpBar = transform.Find("Canvas").Find("Hp Bar");
        var hpPer = (float) _character.Hp() / (float) _character.MaxHp();
        if (isLocalPlayer)
        {
            var slider = GameObject.Find("Hp Bar").gameObject.GetComponent<Slider>();
            hpBar.gameObject.SetActive(false);

            slider.value = hpPer;
        }
        else
        {
            hpBar.gameObject.GetComponent<Slider>().value = hpPer;
        }

        // SP 업데이트
        if (isLocalPlayer)
        {
            var spPer = (float) _character.Sp() / (float) _character.MaxSp();
            var slider = GameObject.Find("Sp Bar").gameObject.GetComponent<Slider>();
            slider.value = spPer;
        }
    }

    public void HandleImpulse(float delta)
    {
        if (_impulse.magnitude > 0.1)
        {
            var force = _impulse;
            force.y = 0;
            _characterController.Move(force * delta);
        }

        var duration = 5;
        _impulse = Vector3.Lerp(_impulse, Vector3.zero, duration*delta);
    }

    public void OnWeaponHit(int damage, Vector3 point, Vector3 normal, float force)
    {
        Vector3 forward = transform.forward;
        normal = -normal;
        float attackCos = Vector3.Dot(forward.normalized, (-normal.normalized));
        float parryCos = Mathf.Cos(ParryableAngle * Mathf.Deg2Rad);
        
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, normal);
        _impulse = normal * force;
        
        if ((_currAnim == AnimatorEvent.StateName.Guard || _currAnim == AnimatorEvent.StateName.Parrying) && attackCos >= parryCos)
        {
            animHandler.Hit(true); 
            var ins = Instantiate(ParryingEffect, point, rot);
            ins.Play();
        }
        else
        {
            Debug.DrawLine(point, point + (normal*force), Color.red, 10);
            Debug.DrawLine(point, point + (transform.forward*force), Color.blue, 10);
            animHandler.Hit(false); 
            var ins = Instantiate(BloodEffect, point, rot);
            ins.Play();
        }
    }

    public void ForwardTarget(float delta)
    {
        if (_playerManager.lockOn != null && (_currAnim == AnimatorEvent.StateName.Attack || _currAnim == AnimatorEvent.StateName.Guard))
        {
            if (isLocalPlayer)
                Debug.Log("Forward Handle");
            var dir = (_playerManager.lockOn.transform.position - transform.position).normalized;
            Quaternion tr = Quaternion.LookRotation(dir);
            Quaternion targetRotation = Quaternion.Lerp(transform.rotation, tr, rotationSpeed * delta);
            transform.rotation = targetRotation;
        }
        else
        {
            if (isLocalPlayer)
                return;
            //               Debug.Log("Forward Not Handle!");
        }
    }
}