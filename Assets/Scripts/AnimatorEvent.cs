﻿using UnityEngine;

public static class AnimatorEvent
{
    public enum StateChangeEvent
    {
        OnEnter,
        OnUpdate,
        OnExit
    }

    public enum StateName
    {
        Locomo,
        Attack,
        Guard,
        Evade,
        Hit,
        Parrying
    }

    public interface IAnimatorEventHandler
    {
        void OnStateChanged(StateName stateName, StateChangeEvent e, AnimatorStateInfo stateInfo, int layerIndex);
    }
}