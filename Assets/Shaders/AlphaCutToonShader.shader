﻿Shader "Custom/TransToonShader"
{
     Properties
    {
        [Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull", Float) = 0
        _MainTex ("Texture", 2D) = "white" {}
        [HDR]
        _Color ("Color", Color) = (0.5,0.5,1,1)
        _CutOff ("CutOff", float) = 0.1

    }
    SubShader
    {
        Pass
        {
            Tags { "RenderType"="Opaque" }
            Tags { "LightMode"="ForwardBase" }
            Cull [_Cull]
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase
            
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
 
            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
                float4 normal : NORMAL;
            };

            struct v2f
            {
                float3 worldNormal : NORMAL;
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };
            
            sampler2D _MainTex;
            //_MainTex_ST has x,y = TexScale, z,w = Texoffset
            float4 _MainTex_ST;
            float4 _Color;
            float4 _Ambient;
            float _CutOff;
            
            float4 frag (v2f i) : SV_Target {
                float3 normal = normalize(i.worldNormal);
                float4 sample = tex2D(_MainTex, i.uv);
                if (sample.w <= _CutOff)
                {
                    discard;
                }

                //_WorldSpaceLightPos0 = Direction Light Position
                float normalDotLight = dot(_WorldSpaceLightPos0, normal);
                float lightIntensity = smoothstep(0, 0.05, normalDotLight);
                float lightColor = lightIntensity * _LightColor0;
                return _Color * sample * (lightColor + _Ambient);
            }
            
            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                return o;
            }
            ENDCG
        }
    }
    
    FallBack "Diffuse"
}
