﻿Shader "Custom/ToonShader"
{
    Properties
    {
        [Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull", Float) = 0
        [HDR]
        _Color ("Color", Color) = (0.5,0.5,1,1)
        _MainTex ("Texture", 2D) = "white" {}
        [HDR]
        _Ambient("Ambient", Color) = (0.5,0.5,1,1)
        [HDR]
        _SpecularColor("Specular", Color) = (0.7,0.7,0.7,1)
        _Glossiness("Glossiness", Float) = 32
        [HDR]
        _RimColor("Rim Color", Color) = (1,1,1,1)
        _RimAmount("Rim Amount", Range(0, 1)) = 0.7
        _RimThreshold("Rim Threshold", Range(0, 1)) = 0.1
    }
    SubShader
    {
        Pass
        {
            Tags { "RenderType"="Opaque" }
            Tags { "LightMode"="ForwardBase" }
            Tags { "PassFlags"="OnlyDirectional" }
            Cull [_Cull]
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase
            
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"
            
            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
                float4 normal : NORMAL;
            };

            struct v2f
            {
                float3 worldNormal : NORMAL;
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 viewDirection : TEXCOORD1;
                SHADOW_COORDS(2) //Get SHADOW TEXTURE TEXCOORD2
            };
            
            sampler2D _MainTex;
            //_MainTex_ST has x,y = TexScale, z,w = Texoffset
            float4 _MainTex_ST;
            float4 _Color;
            float4 _Ambient;
            float _Glossiness;
            float4 _SpecularColor;
            float4 _RimColor;
            float _RimAmount;
            float _RimThreshold;
            
            float4 frag (v2f i) : SV_Target {
                float3 normal = normalize(i.worldNormal);
                float4 sample = tex2D(_MainTex, i.uv);
                
                //viewDirection, frag to camera in worldSpace
                float3 viewDirection = normalize(i.viewDirection);
                float3 reflection = reflect(viewDirection, normal);
                float specular = saturate(dot(reflection, -_WorldSpaceLightPos0));
                
                //_WorldSpaceLightPos0 = Direction Light Position
                float normalDotLight = dot(_WorldSpaceLightPos0, normal);
                float shadow = SHADOW_ATTENUATION(i);
                float lightIntensity = smoothstep(0, 0.05, normalDotLight * shadow);
                float lightColor = lightIntensity * _LightColor0;

                specular = pow(specular * lightIntensity, _Glossiness * _Glossiness);
                float4 specularColor = smoothstep(0.005, 0.01, specular) * _SpecularColor;

                //Rim Color
                float rim = 1 - saturate(dot(normal, viewDirection));
                rim = rim * pow(normalDotLight, _RimThreshold);
                rim = smoothstep(_RimAmount - 0.01, _RimAmount + 0.1, rim);
                float4 rimColor = rim * _RimColor;
                return _Color * sample * (lightColor + _Ambient + specularColor + rimColor);
            }
            
            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                //Calculated With _MainTex_ST
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                //Like mul(Transpose(Inverse(WorldMat)), v.normal)
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.viewDirection = WorldSpaceViewDir(v.vertex);
                TRANSFER_SHADOW(o);
                return o;
            }
            ENDCG
        }
        
        UsePass "VertexLit/SHADOWCASTER"
    }
    
    FallBack "Diffuse"
}
