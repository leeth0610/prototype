﻿Shader "Custom/DisolveShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Disolve Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" }
        Tags { "RenderType"="Transparency" }
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite off
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
                float4 position : SV_POSITION;
                float CustomData1 : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert(appdata i)
            {
                v2f o;
                o.color = i.color;
                o.CustomData1 = i.uv.z;
                o.position = UnityObjectToClipPos(i.vertex);
                o.uv = TRANSFORM_TEX(i.uv, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float customData = i.CustomData1;
                //Sampling
                fixed4 disolve = tex2D(_MainTex, i.uv);
                float4 color;
                color.xyz = i.color.rgb;
                color.w = ceil(disolve.r - customData) * i.color.a;
                return color;
            }
            ENDCG
        }
    }
}
