﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class BasicSword : MonoBehaviour, IWeapon
{
    private GameObject _holder;
    private Action<Collision> _onHitHandler;
    private bool _collisionTesting = false;
    [Header("Stats")] public int LightAttackDamage = 10;
    [Header("Stats")] public float Force = 20;

    public void Init()
    {
        GetComponent<Collider>().enabled = false;
        _initTransform();
    }

    public void SetHolder(GameObject holder)
    {
        _holder = holder;
    }

    public void Attack(Collider otherObject, Vector3 collisionPoint, Vector3 collisionNormal)
    {
        var collider = otherObject;
        var other = collider.GetComponent<CharacterObject>();
        if (other == null)
        {
            return;
        }

        var hitResult = other.Hit(LightAttackDamage, collisionPoint, collisionNormal.normalized, Force);
        if (hitResult.Item1)
        {
            // 데미지 표시    
        }
        else
        {
            
        }
        Debug.Log("Target:" + collider.name + ", Remain HP:" + other.Hp().ToString());
    }

    public void SetOnHitEventHandler(Action<Collision> action)
    {
        _onHitHandler = action;
    }

    public void UpdateAttackState(bool attackState)
    {
        _collisionTesting = attackState;
        GetComponentInChildren<CapsuleCollider>().enabled = attackState;
        GetComponent<DynamicCollider>().Enable(attackState);
    }

    private void OnCollisionEnter(Collision other)
    {
        _onHitHandler(other);
    }

    private void _initTransform()
    {
        if (_holder == null)
        {
            //Use Default position
            return;
        }
        gameObject.transform.position = _holder.transform.position;
        gameObject.transform.rotation = _holder.transform.rotation;
    }

    private void Update()
    {
        _initTransform();
    }
}