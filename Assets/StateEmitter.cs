﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateEmitter : StateMachineBehaviour
{
    public AnimatorEvent.StateName stateName;
    private static AnimatorEvent.IAnimatorEventHandler[] _handlers;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _emitStateChanged(animator, AnimatorEvent.StateChangeEvent.OnEnter, stateInfo, layerIndex);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _emitStateChanged(animator, AnimatorEvent.StateChangeEvent.OnUpdate, stateInfo, layerIndex);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _emitStateChanged(animator, AnimatorEvent.StateChangeEvent.OnExit, stateInfo, layerIndex);
    }

    private static AnimatorEvent.IAnimatorEventHandler[] _obtainHandlers(Component animator)
    {
        return animator.GetComponents<AnimatorEvent.IAnimatorEventHandler>();;
    }

    private void _emitStateChanged(Animator animator, AnimatorEvent.StateChangeEvent e, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var handlers = _obtainHandlers(animator);
        foreach (var h in handlers)
        {
            h.OnStateChanged(stateName, e, stateInfo, layerIndex);
        }
    }
}