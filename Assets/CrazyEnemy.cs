﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror.SimpleWeb;
using UnityEngine;

public class CrazyEnemy : MonoBehaviour
{
    // Start is called before the first frame update
    public float RotationSpeed = 500;
    private IWeapon _iweapon;
    private GameObject _weapon;
    public GameObject weaponHolder;
    
    void Start()
    {
        
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            var child = gameObject.transform.GetChild(i);
            if (child.CompareTag("Weapon"))
            {
                _weapon = child.gameObject;
            }
        }

        _iweapon = _weapon.GetComponent<IWeapon>();
        _iweapon.SetHolder(weaponHolder);
        _iweapon.SetOnHitEventHandler(_onWeaponHit);
        _iweapon.Init(); 
        GetComponentInChildren<DynamicCollider>().Enable(true);
    }

    // Update is called once per frame
    private void Update()
    {
        gameObject.transform.Rotate(new Vector3(0, -RotationSpeed , 0) * Time.deltaTime);
    }
    
    private void _onWeaponHit(Collision collision)
    {
        var collider = collision.collider;
        if (collider.GetComponent<Collider>().gameObject.Equals(gameObject))
        {
            return;
        }
        var contact = collision.GetContact(0);
        
        _iweapon.Attack(collider, contact.point, contact.normal);
    }
}
